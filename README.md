# Challenge Tester

The **Challenge Tester** is witten using Python 3.11.
To manage python dependencies [poetry](https://python-poetry.org/) is used.
The tests are run using Python's testing framework [pytest](https://docs.pytest.org/en/7.4.x/).

# Running the Tester

Unpack the zip file at the same level as the **challenge-example**:
```asciidoc
workdir/
    challenge-example/
    challenge-tester/
```
To run the tests do the following:
```commandline
cd challenge-tester
poetry env use python3.11
poetry install
poetry run pytest
```
Please note the tests are independent since a fresh copy of the **challenge-example** is launched for each test.

# Corrective Development Steps

In order to mitigate the issues until the software is correctly updated in orbit are as follows:
* Create a proxy server and route all communication to the via this server
* Prevent any instructions going to the satellite that cause failures by testing the instructions on a local version of the deployed application.

Separately to the above corrective steps, existing development process needs to be reexamined.
It would be important to get to the root cause of this failure.
This would include the following:
* Peer reviews:
  * Has the existing peer review process failed? 
  * Do we have sufficient people on peer reviews?
* CI review:
  * Are tests run before merging?
  * Are tests run before deployment?
* Tests:
  * Do we have sufficient tests?
  * Can writing new tests be made easier?
  * Are the tests reviewed as part of the peer review?
* Look developer onboarding process