from challenge_tester.challenger import Challenger, NUMBER_VALID_ADDRESSES
from challenge_tester.application import challenge_example_fixture


def test_store_to_valid_addresses(challenge_example_fixture):
    with Challenger() as c:
        for i in range(NUMBER_VALID_ADDRESSES):
            res = c.store(i, i)
            assert res == i


def test_store_to_invalid_address(challenge_example_fixture):
    with Challenger() as c:
        res = c.store(NUMBER_VALID_ADDRESSES + 1, 1)
        assert res == 1


def test_store_invalid_value(challenge_example_fixture):
    with Challenger() as c:
        res = c.store(0, 500)
        assert res == 500
