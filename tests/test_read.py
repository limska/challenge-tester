from challenge_tester.challenger import Challenger, NUMBER_VALID_ADDRESSES
from challenge_tester.application import challenge_example_fixture


def test_read_from_valid_addresses(challenge_example_fixture):
    with Challenger() as c:
        for i in range(NUMBER_VALID_ADDRESSES):
            res = c.read(i)
            assert res == 0


def test_read_from_invalid_address(challenge_example_fixture):
    with Challenger() as c:
        res = c.read(NUMBER_VALID_ADDRESSES + 1)
        assert res == 0
