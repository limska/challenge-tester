from challenge_tester.challenger import Challenger, NUMBER_VALID_ADDRESSES
from challenge_tester.application import challenge_example_fixture


def test_valid_sum(challenge_example_fixture):
    with Challenger() as c:
        total = 0
        for i in range(NUMBER_VALID_ADDRESSES):
            res = c.store(i, i)
            assert res == i
            res = c.read(i)
            assert res == i
            total += i

        res = c.sum()
        assert res == total
