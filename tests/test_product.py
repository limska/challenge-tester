from challenge_tester.challenger import Challenger, NUMBER_VALID_ADDRESSES
from challenge_tester.application import challenge_example_fixture


def test_valid_product(challenge_example_fixture):
    with Challenger() as c:
        total = 1
        for i in range(NUMBER_VALID_ADDRESSES):
            value = 2 * i
            res = c.store(i, value)
            assert res == value
            res = c.read(i)
            assert res == value
            total *= value

        res = c.product()
        assert res == total


def test_invalid_product(challenge_example_fixture):
    with Challenger() as c:
        total = 1
        for i in range(NUMBER_VALID_ADDRESSES):
            value = 10 * i
            res = c.store(i, value)
            assert res == value
            res = c.read(i)
            assert res == value
            total *= value

        res = c.product()
        assert res == total
