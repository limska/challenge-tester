from challenge_tester.challenger import Challenger


def run_tests():
    with Challenger() as c:
        c.store(0, 1)
        c.store(1, 1)
        c.state()
        c.read(0)
        c.sum()
        c.product()
