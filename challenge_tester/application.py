import concurrent.futures
import subprocess
import time
import pytest
from pathlib import Path

PROGRAM_STARTED = "Program started, waiting for connections"


def run_challenge_example():
    command = ['cargo', 'run']
    challenge_example_directory = Path(__file__).resolve().parents[2] / "challenge-example"

    # Create subprocess and redirect stdout and stderr to pipes
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        cwd=challenge_example_directory.resolve().absolute()
    )

    return process


def launch_and_monitor_challenge_example():
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(run_challenge_example)

    return future


@pytest.fixture
def challenge_example_fixture():
    # Start the external application as a background thread
    future = launch_and_monitor_challenge_example()

    process = future.result()

    while process.poll() is None:
        # Read stdout and stderr
        stdout_line = process.stdout.readline().strip()

        # Process and print stdout
        if stdout_line and stdout_line == PROGRAM_STARTED:
            break

        # Add a short delay to avoid high CPU usage
        time.sleep(0.05)

    yield future.result()

    if not future.done():
        # Cancel the future (terminate the external application)
        process.kill()
        future.cancel()
        try:
            # Wait for the external application to be terminated
            future.result()
        except concurrent.futures.CancelledError:
            pass
