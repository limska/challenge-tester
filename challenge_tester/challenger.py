import errno
import socket
import select
import time
from typing import NamedTuple
from enum import Enum
from socket import error as SocketError

NUMBER_VALID_ADDRESSES = 4
READ_CMD = 1
STORE_CMD = 2
SUM_CMD = 3
PRODUCT_CMD = 4
APP_ADDRESS = '127.0.0.1'
APP_PORT = 10203
RECEIVE_BUFFER_SIZE = 1024
INT_SIZE = 4


class Status(Enum):
    SUCCESS = 0
    FAILURE = 1


class Response(NamedTuple):
    status: Status
    value: int


class CommunicationError(Exception):
    def __init__(self, message="Communication Error"):
        self.message = message
        super().__init__(self.message)


class Challenger:
    def __init__(self):
        self.s = None

    def __enter__(self):
        return self

    def _connect(self):
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((APP_ADDRESS, APP_PORT))
            # Set socket to non blocking
            self.s.setblocking(False)
        except Exception as e:
            raise CommunicationError(f"Something went wrong while connecting to {APP_ADDRESS}:{APP_PORT}: {e}")

    def __exit__(self, exc_type, exc_value, traceback):
        self._close()

    def _close(self):
        try:
            if self.s is not None:
                self.s.close()
            self.s = None
        except Exception as e:
            raise CommunicationError(f"Something went wrong while closing: {e}")

    def _exchange(self, *args):
        data = bytearray()
        for arg in args:
            data.append(arg)

        self._connect()

        inputs = [self.s]
        outputs = [self.s]
        response = []
        while inputs:
            readable, writable, errors = select.select(inputs, outputs, inputs, 0.2)

            for s in writable:
                s.send(data)
                outputs.remove(s)

            for s in readable:
                data = s.recv(RECEIVE_BUFFER_SIZE)
                response.extend(data)
                inputs.remove(s)

            for s in errors:
                self._close()
                raise CommunicationError(f"Something went wrong while receiving on: {s}")

        self._close()

        if len(response) < 2:
            raise CommunicationError(f"Failed to receive valid response: {response}")

        try:
            res = Response(Status(response[0]), response[1])
        except Exception as e:
            raise CommunicationError(f"Format not valid: {e} {response}")

        return res

    def read(self, loc):
        loc_as_bytes = loc.to_bytes(INT_SIZE, byteorder='little', signed=False)
        res = self._exchange(READ_CMD, loc_as_bytes[0])
        if res.status == Status.FAILURE:
            raise ValueError("Error while reading value")
        return res.value

    def store(self, loc, value):
        loc_as_bytes = loc.to_bytes(INT_SIZE, byteorder='little', signed=False)
        value_as_bytes = value.to_bytes(INT_SIZE, byteorder='little', signed=False)
        res = self._exchange(STORE_CMD, loc_as_bytes[0], value_as_bytes[0])
        if res.status == Status.FAILURE:
            raise ValueError("Error while storing value")
        return res.value

    def sum(self):
        res = self._exchange(SUM_CMD)
        if res.status == Status.FAILURE:
            raise ValueError("Error while sending sum command")
        return res.value

    def product(self):
        res = self._exchange(PRODUCT_CMD)
        if res.status == Status.FAILURE:
            raise ValueError("Error while sending product command")
        return res.value
